#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include "SSD1306.h"
#include "DHT.h"

// Define serial rate
#define SERIAL_RATE 115200

// Define DHT sensor type
#define DHTTYPE DHT22

// Define output pin of DHT sensor
#define TEMP_PIN D4
#define DHTPin D4

// Initialize DHT sensor.
DHT dht(DHTPin, DHTTYPE);

// Define the OLED pins
#define SDA D5
#define SDC D6
#define OLED_ADDRESS 0x3c

// Initialize the OLED display using Wire library
SSD1306 display(OLED_ADDRESS, SDA, SDC);

// Define other output pins
#define BLUE_PIN D1
#define GREEN_PIN D2
#define RED_PIN D3

// Json message length
#define JSON_LEN 100

// Loop delay time (millisecond)
#define LOOP_DELAY_MILL 120000

// Ping delay time (millisecond)
#define PING_DELAY_MILL 60000

// Check connection (millisecond)
#define CONN_DELAY_MILL 30000

// Loop delay time (second)
#define LOOP_DELAY_SEC 1

// Accumulate time
#define ACCU_TIME 30

// Broker port
#define BROKER_PORT 4883

// User Id
#define USER_ID "583a3dd05cc76613049a0003"

// Device Id
#define DEVICE_ID "device_smartboilerv01"

// Device subscribe topic
#define SUB_TOPIC "smartboiler/command"

// Device subscribe topic
#define PING_SUB_TOPIC "smartboiler/command/ping"

// Device subscribe topic
#define PINS_SUB_TOPIC "smartboiler/command/pins"

// Device publish topic
#define PUB_TOPIC "smartboiler/result"

// Device publish topic
#define THERMO_PUB_TOPIC "smartboiler/result/thermo"

// Device publish topic
#define PING_PUB_TOPIC "smartboiler/result/ping"

// Device publish topic
#define PINS_PUB_TOPIC "smartboiler/result/pins"

// Message type for json
#define TEMP_JSON 0
#define GET_PIN_JSON 1

// Define LCD info code
#define LCD_MSG 0
#define LCD_ERR 1
#define LCD_PIN 2
#define LCD_VAL 3
#define ERR_PREFIX "ERR:"
#define MSG_PREFIX "MSG:"
#define PIN_PREFIX "P:"
#define VAL_PREFIX "V:"
#define ERR_CONNECT 40 // Cannot connect to network
#define MSG_GETPIN 20 // Get pin status
#define MSG_SETPIN 30 // Set pin status

// Network SSID (name)
char ssid[] = "Teksavvy3471";

// Network password
char pass[] = "downtown999";

// Broker server ip
// char server[] = "www.grangle.net";
IPAddress server(192, 168, 1, 11);

// Callback function header
void callback(char* topic, byte* payload, unsigned int length);

// Initialize the client library
WiFiClient wifiClient;
PubSubClient client(server, BROKER_PORT, callback, wifiClient);

// Will store last time send data
static unsigned long previousMillis = 0;
static unsigned long pingMillis = 0;
static unsigned long connMillis = 0;

// Temporary variables
static boolean isConnected;
static float humidity;
static float celsius;
static float fahrenheit;
static float heatIndexCelsius;
static float heatIndexFahrenheit;

/**
 * Setup Serial
 */
void setupSerial()
{
    // Open serial communications and wait for port to open
    Serial.begin(SERIAL_RATE);
    while (!Serial) {
        ; // Wait for serial port to connect. Needed for native USB port only
    }
}

/**
 * Setup wifi
 */
void setupWifi()
{
    delay(10);

    Serial.println();
    Serial.print("Connecting to ");
    Serial.print(ssid);

    WiFi.begin(ssid, pass);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println();
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

/**
 * Setup OLED
 */
void setupOLED()
{
    Serial.println("Setup OLED");

    // Initialising the UI will init the display too.
    display.init();

    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_10);

    // clear the display
    display.clear();
}

/**
 * Setup DHT sensor
 */
void setupDHT()
{
    Serial.println("Setup DHT");

    dht.begin();
}

/**
 * Setup LED pins
 */
void setupLED()
{
    Serial.println("Setup LED");

    // Set led pin
    pinMode(BLUE_PIN, OUTPUT);
    pinMode(GREEN_PIN, OUTPUT);
    pinMode(RED_PIN, OUTPUT);
}

/**
 * Print IP Address to OLED
 */
void printIPAddress()
{
    String ip = String("IP : ");

    for (byte thisByte = 0; thisByte < 4; thisByte++) {
        // Print the value of each byte of the IP address:
        ip.concat(WiFi.localIP()[thisByte]);
        if (thisByte < 3) {
            ip.concat(".");
        }
    }

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, ip);
    display.display();
}

/**
 * Print Thermo info
 */
void printThermoInfo()
{
    // Print label
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 10, "H [ % ]");

    // Print humidity value
    String h = String(humidity);
    display.drawString(0, 20, h);

    // Print label
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 10, "T [ *C ]");

    // Print temperature in Celsius
    String hic = String(heatIndexCelsius);
    display.drawString(64, 20, hic);

    // Print label
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(128, 10, "T [ *F ]");

    // Print temperature in Fahrenheit
    String hif = String(heatIndexFahrenheit);
    display.drawString(128, 20, hif);
}

/**
 * Print connection status
 */
void printConnectStatus() {
    // Print label
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    if(isConnected) {
        display.drawString(0, 30, "Connected");   
    }
    else {
        display.drawString(0, 30, "Connecting...");        
    }
}

/**
 * Print information to OLED
 */
void printToOLED()
{
    display.clear();
    printIPAddress();
    printThermoInfo();
    printConnectStatus();
    display.display();
}

/**
 * Read thermo sensor
 */
void thermoSensor()
{
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    humidity = dht.readHumidity();
    // Read temperature as Celsius (the default)
    celsius = dht.readTemperature();
    // Read temperature as Fahrenheit (isFahrenheit = true)
    fahrenheit = dht.readTemperature(true);

    // Check if any reads failed and exit early (to try again).
    if (isnan(humidity) || isnan(celsius) || isnan(fahrenheit)) {
        Serial.println("Failed to read from DHT sensor!");
        return;
    }

    // Compute heat index in Fahrenheit (the default)
    heatIndexFahrenheit = dht.computeHeatIndex(fahrenheit, humidity);
    // Compute heat index in Celsius (isFahreheit = false)
    heatIndexCelsius = dht.computeHeatIndex(celsius, humidity, false);

    Serial.print("Humidity: ");
    Serial.println(humidity);
    Serial.print("Temperature Celsius: ");
    Serial.print(celsius);
    Serial.println(" *C");
    Serial.print("Heat index Celsius: ");
    Serial.print(heatIndexCelsius);
    Serial.println(" *C");
    Serial.print("Temperature Fahrenheit: ");
    Serial.print(fahrenheit);
    Serial.println(" *F");
    Serial.print("Heat index Fahrenheit: ");
    Serial.print(heatIndexFahrenheit);
    Serial.println(" *F");
}

/**
 * Convert thermo data to JSON
 */
void thermoJson(char* json)
{
    // Determine the memory usage of this object tree
    const int BUFFER_SIZE = JSON_OBJECT_SIZE(5) + JSON_ARRAY_SIZE(0);

    // Reserve memory space
    StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;

    // Build object tree in memory
    JsonObject& root = jsonBuffer.createObject();
    root["uid"] = USER_ID; // User Id
    root["tp"] = TEMP_JSON; // tp = 0
    root["hu"] = humidity; // Humidity value
    root["cs"] = heatIndexCelsius; // Celsiys degree
    root["fa"] = heatIndexFahrenheit; // Fahrenheit degree

    // Print out for testing
    root.printTo(json, JSON_LEN);
}

/**
 * Get pins value
 */
void getPins(JsonArray& inPins)
{
    // Determine the memory usage of this object tree
    const int BUFFER_SIZE = JSON_OBJECT_SIZE(4) + JSON_ARRAY_SIZE(10);

    // Reserve memory space
    StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;

    // Build object tree in memory
    JsonObject& root = jsonBuffer.createObject();
    root["tp"] = GET_PIN_JSON;

    JsonArray& pins = root.createNestedArray("pin");
    JsonArray& vals = root.createNestedArray("val");
    JsonArray::iterator itPin = inPins.begin();
    int pin, val;

    for (; itPin != inPins.end(); ++itPin) {
        pin = *itPin;
        pins.add(pin);

        val = digitalRead(pin);
        vals.add(val);
    }

    // Prepare json message
    char json[JSON_LEN];

    // Print out for testing
    root.printTo(json, JSON_LEN);

    // Serial.println("Response");
    // root.printTo(Serial);
    // Serial.println("");

    // Publish information to server
    publish(json);
}

/**
 * Set pin value
 */
void setPin(int pin, int val)
{
    if (val == 1) {
        digitalWrite(pin, HIGH);
    }
    else {
        digitalWrite(pin, LOW);
    }
}

/**
 * MQTT server connection
 */
void connectBroker()
{
    // Keep connect to broker server
    if (client.connected()) {
        client.loop();
        isConnected = true;
    }
    else {
        // Not connected then connect to broker
        Serial.println("Device isn't connected. Connecting...");
        if (client.connect(DEVICE_ID)) {
            Serial.println("Device connected.");
            // After connect subcribe to inTopic
            client.subscribe(SUB_TOPIC);
            client.subscribe(PING_SUB_TOPIC);
            client.subscribe(PINS_SUB_TOPIC);
            isConnected = true;
        }
        else {
            Serial.println("Cannot connect.");
            isConnected = false;
        }
    }
}

/*
 * Callback function
 * When the server send message, we will get the payload here
 * The payload is in json format so we will call commandHandler
 * to parse the payload and decide what to do after that
 */
void callback(char* topic, byte* payload, unsigned int length)
{
    // In order to republish this payload, a copy must be made
    // as the orignal payload buffer will be overwritten whilst
    // constructing the PUBLISH packet.

    // Allocate the correct amount of memory for the payload copy
    byte* p = (byte*)malloc(length + 1);

    // Copy the payload to the new buffer
    memcpy(p, payload, length);
    p[length] = '\0';

    commandHandler(p);

    // Free the memory
    free(p);
}

/**
 * List of command
 */
void commandHandler(byte* payload)
{
    // Determine the memory usage of this object tree
    const int BUFFER_SIZE = JSON_OBJECT_SIZE(4) + JSON_ARRAY_SIZE(10);

    // Reserve memory space
    StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;

    JsonObject& root = jsonBuffer.parseObject((char*)payload);

    // Serial.println("Payload");
    // root.printTo(Serial);
    // Serial.println("");

    if (root.success() && root.containsKey("type") && root.containsKey("pins") && root.containsKey("vals")) {
        int type = root["type"], pin, val;
        JsonArray& pins = root["pins"].asArray();
        JsonArray& vals = root["vals"].asArray();
        JsonArray::iterator itPin = pins.begin();
        JsonArray::iterator itVal = vals.begin();

        if (type == 1) {
            // Type 1: Get pins status
            getPins(pins);
        }
        else if (type == 2) {
            // Type 2: Set pins status
            for (; itPin != pins.end(), itVal != vals.end(); ++itPin, ++itVal) {
                val = *itVal;
                pin = *itPin;
                setPin(pin, val);
                printInfo(1, 0, LCD_MSG, MSG_SETPIN);
                printInfo(1, 7, LCD_PIN, pin);
                printInfo(1, 11, LCD_VAL, val);
            }

            getPins(pins);
        }
    }
}

/**
 * Publish to out topic
 */
void publish(char* json)
{
    // Check broker connection status
    if (!client.connected()) {
        // Serial.println("Device isn't connected. Connecting...");
        if (client.connect(DEVICE_ID)) {
            // Serial.println("Device connected.");
            client.publish(PINS_PUB_TOPIC, json);
            client.subscribe(SUB_TOPIC);
            client.subscribe(PING_SUB_TOPIC);
            client.subscribe(PINS_SUB_TOPIC);
        }
        else {
            // Serial.println("Cannot connect (publish).");
        }
    }
    else {
        // Serial.println("Device connected.");
        client.publish(PINS_PUB_TOPIC, json);
    }
}

/**
 * Print msg code to oled
 */
void printInfo(int row, int col, int type, int code)
{
    /*lcd.setCursor(col, row);

    if (type == LCD_MSG) {
        lcd.print(MSG_PREFIX);
    }
    else if (type == LCD_ERR) {
        lcd.print(ERR_PREFIX);
    }
    else if (type == LCD_PIN) {
        lcd.print(PIN_PREFIX);
    }
    else {
        lcd.print(VAL_PREFIX);
    }

    lcd.print(code);*/
}

void setup()
{
    // Setup Serial for debug
    setupSerial();

    // Setup DHT
    setupDHT();

    // Setup LED
    setupLED();

    // Setup OLED
    setupOLED();

    // Setup Wifi
    setupWifi();

    // Print your local IP address
    printIPAddress();

    // Connect to broker when setup
    connectBroker();
    thermoSensor();

    printToOLED();
}

void loop()
{
    unsigned long currentMillis = millis();

    if (client.connected()) {
        // Keep connection alive
        client.loop();
        isConnected = true;

        // Send ping msg
        if (currentMillis - pingMillis >= PING_DELAY_MILL) {
            // Save the last time
            pingMillis = currentMillis;

            // Sense temperature
            thermoSensor();
            printToOLED();

            client.publish(PING_PUB_TOPIC, "1");
        }

        // Send temperature msg
        if (currentMillis - previousMillis >= LOOP_DELAY_MILL) {
            // Save the last time
            previousMillis = currentMillis;

            // Sense temperature
            thermoSensor();
            printToOLED();

            // Prepare json message
            char json[JSON_LEN];
            thermoJson(json);

            // Publish data
            client.publish(THERMO_PUB_TOPIC, json);
        }
    }
    else {
        if (currentMillis - connMillis >= CONN_DELAY_MILL) {
            // Save the last time
            connMillis = currentMillis;

            // Sense temperature
            thermoSensor();
            printToOLED();

            connectBroker();
        }
    }
}

