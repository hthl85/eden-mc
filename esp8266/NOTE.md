# Setup ESP8266 Arduino

## Install driver

- Download the driver and follow the `readme` file in this link `http://raysfiles.com/drivers/ch341ser_mac.zip`

## Config Arduino IDE

- Follow this link `http://www.instructables.com/id/Quick-Start-to-Nodemcu-ESP8266-on-Arduino-IDE/`

## Required libraries

- ### `ArduinoJson`
- ### `PubSubClient`
- ### `SSD1306` (for OLED)
- ### `DHT` (for temperature sensor)
